package cn.tsingyu.antsclub;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tsingyu.antsclub.bean.Trade;
import cn.tsingyu.antsclub.service.TradeService;
import cn.tsingyu.antsclub.util.ExcelUtil;
import jxl.Workbook;
import jxl.write.WritableWorkbook;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TradeTests {
	@Autowired
	private TradeService tradeSerivce;
	@Before
	public void setUp() {
		// 准备，清空user表
		//userSerivce.deleteAllUsers();
	}
	public void test() throws Exception {
		List<Trade> list = tradeSerivce.getRecord("2014-06-25 18:56:26", "2016-06-25 18:56:26");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getSourcename());
		}
		System.out.println(list.size());
	}
	
	public void test1() throws Exception{
		String[][] array = tradeSerivce.getRecordArray("2014-06-25 18:56:26", "2016-06-25 18:56:26");
		for (int i = 0; i < array.length; i++) {
			String[] row = array[i];
			for(int j=0;j<row.length;j++){
				System.out.println(i+j+row[j]);
			}
		}
		System.out.println(array.length);
	}
	
	@Test
	public void test2() throws Exception{
		String excelName = "D:/export2003_"+ System.currentTimeMillis() + ".xls";
		ExcelUtil ce = new ExcelUtil();
		String[] header = { "交易类型", "交易金额", "付款人", "备注", "操作时间", "操作人"};
		String[][] data = tradeSerivce.getRecordArray("2016-06-25 18:56:26", "2016-07-25 18:56:26");
		String[] footer = { "交易类型", "交易金额", "付款人", "备注", "操作时间", "操作人"};
		File excelFile = new File(excelName);
		// 如果文件存在就删除它
		if (excelFile.exists())
			excelFile.delete();
		// 打开文件
		WritableWorkbook book;
		try {
			book = Workbook.createWorkbook(excelFile);
			ce.exportDailyPlanTemplate(book,excelName,header,data,footer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
