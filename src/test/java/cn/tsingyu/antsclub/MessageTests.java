package cn.tsingyu.antsclub;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tsingyu.antsclub.service.MessageService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MessageTests {
	@Autowired
	private MessageService messageService;
	
	public void test1() throws Exception {
		messageService.generateVerificationCode("15950014490", "register");
	}
	@Test
	public void test2() throws Exception {
		String code = messageService.getVerificationCode("15950014490", "register");
		System.out.println("getCode:"+code);
	}
	
}
