package cn.tsingyu.antsclub;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tsingyu.antsclub.service.LoginService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LoginTests {
	@Autowired
	private LoginService loginService;
	
	@Test
	public void testCheckPhone() throws Exception {
		loginService.checkMobile("15950014490");
		Assert.assertEquals(false,loginService.checkMobile("15950014490"));
		Assert.assertEquals(false,loginService.checkName("15950014490"));
	}
	
}
