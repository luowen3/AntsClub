package cn.tsingyu.antsclub.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tsingyu.antsclub.annotation.RequiresPermissions;

@Controller
@RequestMapping("/user")
public class UserController {

	/**
	 * 用户查询.
	 * @return
	 */
	@RequestMapping({"","/userList"})
	public String userInfo(){
		return "user/userInfo";
	}
	
	/**
	 * 用户添加;
	 * @return
	 */
	@RequestMapping("/userAdd")
	@RequiresPermissions("userInfo:add")//权限管理;
	public String userAdd(){
		return "user/userAdd";
	}
	
	/**
	 * 用户删除;
	 * @return
	 */
	@RequestMapping("/userDel")
	@RequiresPermissions("userInfo:del")//权限管理;
	public String userDel(){
		return "user/userDel";
	}
}