package cn.tsingyu.antsclub.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tsingyu.antsclub.service.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	private LoginService loginSerivce;
	
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public String login() {
        return "login";
    }
    
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(HttpServletRequest request, Map<String, Object> map) throws Exception {
		String msg = "";
		String url = "/index";
		map.put("msg", msg);
		return url;
	}
	@RequestMapping(value="/register",method=RequestMethod.GET)
	public String register(){
		return "register";
	}
	@RequestMapping(value="/register",method=RequestMethod.POST)
	public String runRegister(){
		return "register";
	}
	/**
	 * 用户手机校验
	 */
	@RequestMapping("checkMobile")
	@ResponseBody
	public Map<String, String> checkMobile(HttpServletRequest request){
		String mobile = request.getParameter("param");
		Boolean canUseFlag = loginSerivce.checkMobile(mobile);
		Map<String , String> map = new HashMap<String, String>();  
		if(canUseFlag){
			map.put("info", "该号码可用");  
		    map.put("status", "y");
		}else{
			map.put("info", "号码已使用，忘记密码可找回");  
		    map.put("status", "n");
		}
		return map;
	}
}