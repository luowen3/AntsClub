package cn.tsingyu.antsclub.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {


    @RequestMapping({"/","/index"})
    public String index(ModelMap map) {
        map.addAttribute("host", "http://www.tsingyu.cn");
        return "index";
    }
    
    @RequestMapping("/msg")
    public String msg(HttpServletRequest request) {
    	request.setAttribute("msg", request.getSession().getAttribute("msg"));
        return "msg";
    }
}