package cn.tsingyu.antsclub.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.tsingyu.antsclub.service.TradeService;
import jxl.Workbook;
import jxl.write.WritableWorkbook;

@Controller
public class TradeController {
	@Autowired
	private TradeService tradeSerivce;

    @RequestMapping("/export")
    public void msg(HttpServletRequest request,HttpServletResponse response) {
    	String excelName = "export2003_"+ System.currentTimeMillis() + ".xls";
    	response.setContentType("application/x-excel;charset=UTF-8");
		response.addHeader("Content-Disposition", "attachment;filename=" + excelName);
		WritableWorkbook book = null;
		try{
			// create book
			book = Workbook.createWorkbook(response.getOutputStream());
			tradeSerivce.exportRechargeRecord(book,excelName,"2015-06-25 18:56:26", "2016-06-25 18:56:26");
		}catch (Exception e) {
			e.printStackTrace();
		}
    	//request.setAttribute("msg", "导出成功");
        //return "msg";
    }
    
}