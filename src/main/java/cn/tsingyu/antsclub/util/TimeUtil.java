package cn.tsingyu.antsclub.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {
	public static String getCurrentTime(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		// new Date()为获取当前系统时间
		return df.format(new Date());
	}
	/**
	 * 当前时间加减一段时间
	 * @param minutes
	 * @return
	 */
	public static String getSomeMinutesBeforeOrAfter(int minutes){
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Calendar c=Calendar.getInstance();
	    c.add(Calendar.MINUTE, minutes);
	    return sdf.format(c.getTime());
	}
	

}
