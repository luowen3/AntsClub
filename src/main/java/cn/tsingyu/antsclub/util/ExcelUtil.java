package cn.tsingyu.antsclub.util;

import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * 导出Excel
 * @author 土龙 516928192@qq.com
 * AntsClub_社群管理系统
 * 2016年12月21日
 */
public class ExcelUtil {
	protected String getFileName(String fileName)
	{
		if (fileName != null)
		{
			String fileNameNew = fileName;
			int index = fileNameNew.lastIndexOf(".");
			if ((index > 0)
				&& (((index == fileNameNew.length() - 4) || (index == fileNameNew.length() - 5))))
			{
				String name = fileNameNew.substring(
					0,
					index);
				fileName = name + ".xls";
			}
			else
			{
				fileName = fileName + ".xls";
			}
		}
		else
		{
			fileName = "d:\\excel_export.xls";
		}
		return fileName;
	}

	public boolean exportDailyPlanTemplate(WritableWorkbook book,String excelName,String[] header,String[][] data,String[] footer ){
		excelName = getFileName(excelName);
		try {
			// 生成名为“充值记录”的工作表，参数0表示这是第一页
			WritableSheet sheet = book.createSheet("充值记录", 0);

			// 在Label对象的构造子中指名单元格位置是第一列第一行(0,0)
			for (int i = 0; i < header.length; i++) {
				Label label = new Label(i, 0, header[i]);
				// 将定义好的单元格添加到工作表中
				sheet.addCell(label);
			}
			// 添加内容
			int dataLength = data.length;
			for (int i = 0; i < dataLength; i++) {
				String[] row = data[i];
				for (int j = 0; j < row.length; j++) {
					Label label = new Label(j, i+1, row[j]);
					// 将定义好的单元格添加到工作表中
					sheet.addCell(label);
				}
			}
			// 添加尾部汇总内容
			for (int i = 0; i < footer.length; i++) {
				Label label = new Label(i, 3+dataLength, footer[i]);
				// 将定义好的单元格添加到工作表中
				sheet.addCell(label);
			}
			// 分别给2,4列设置不同的宽度;
			sheet.setColumnView(1, 10);
			sheet.setColumnView(2, 18);
			sheet.setColumnView(4, 25);
			sheet.setColumnView(5, 15);
			// 写入数据并关闭文件
			book.write();
			book.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
