package cn.tsingyu.antsclub.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

import cn.tsingyu.antsclub.bean.User;
import cn.tsingyu.antsclub.util.TimeUtil;
/**
 * 
 * @author 土龙 516928192@qq.com
 * AntsClub_社群协作系统
 * 2016年12月28日
 */
@Service
public class MessageService{

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
	private Environment env;
    
    /**
     * 生成验证码，用于发送和保存到数据库
     * @param mobile
     * @param type
     */
    public void generateVerificationCode(String mobile,String type) {
    	MessageService ms = new MessageService();
		  //官网的URL---必须是这个
        String url=env.getProperty("message.url"); 

        //成为开发者，创建应用后系统会自动生成
        String appkey=env.getProperty("message.appkey"); 

        //创建应用后系统会自动生成
        String secret=env.getProperty("message.secret");
        //随机生成 num 位验证码
        String code="";
        Random r = new Random(new Date().getTime());
        for(int i=0;i<4;i++){
            code = code+r.nextInt(10);
        }
        //将验证码保存进数据库 用于验证
    	String sql =" insert into verification(mobile, type, code, createTime) values(?,?,?,?) ";
    	jdbcTemplate.update(sql, mobile,type,code,TimeUtil.getCurrentTime());
    	
		TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		req.setExtend("123456");
		req.setSmsType("normal");
		req.setSmsFreeSignName("精羽门");
		req.setSmsParamString("{\"code\":\""+code+"\"}");
		req.setRecNum(mobile);
		req.setSmsTemplateCode("SMS_25475390");
		AlibabaAliqinFcSmsNumSendResponse rsp;
		try {
			rsp = client.execute(req);
			System.out.println(rsp.getBody());
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    /**
     * 活动验证码，用于验证
     * @param mobile
     * @param type
     */
    public String getVerificationCode(String mobile,String type) {
    	
		String sql = "select code from verification where mobile = ? and type = ? "
				+ " and createTime > '" + TimeUtil.getSomeMinutesBeforeOrAfter(-30)+"' "
				+ " order by createTime desc limit 1";
		String code ;
		try{
			code =  jdbcTemplate.queryForObject(sql,new Object[] {mobile,type},java.lang.String.class);
		}catch (EmptyResultDataAccessException e) {
			code = "" ;
		}
		return code;
    }
}
