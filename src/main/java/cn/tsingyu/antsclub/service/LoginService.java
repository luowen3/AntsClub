package cn.tsingyu.antsclub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class LoginService{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 查看手机号是否可以注册
     * @param mobile
     * @return 可用返回true，不可用返回false
     */
    public Boolean checkMobile(String mobile) {
    	
		String sql = "select count(0) from user where uname = ? or mobile = ? ";
		try{
			int count =jdbcTemplate.queryForObject(sql,new Object[] {mobile,mobile},java.lang.Integer.class);
			if(count==0){
				return true;
			}
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		return false;
    }
    /**
     * 查看用户名是否可用
     * @param name
     * @return 可用返回true，不可用返回false
     */
    public Boolean checkName(String name){
    	String sql = "select count(0)  from user where uname = ? or nickname = ? or mobile=? ";
		try{
			int count =jdbcTemplate.queryForObject(sql,new Object[] {name,name,name},java.lang.Integer.class);
			if(count==0){
				return true;
			}
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
    	return false;
    }
}
