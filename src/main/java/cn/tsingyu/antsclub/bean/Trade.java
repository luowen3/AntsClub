package cn.tsingyu.antsclub.bean;

import java.io.Serializable;
import java.util.Date;
public class Trade implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String issue;
	private int tradetype;
	private String tradeamount;
	private int sourceid;
	private String sourcename;
	private int targetid;
	private String targetname;
	private int activityid;
	private String activityname;
	private Date submittime;
	private int paytype;
	private String remark;
	private String createtime;
	private int createuserid;
	private String createusername;
	private Date updatetime;
	private int updateuserid;

	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setIssue(String issue){
		this.issue=issue;
	}
	public String getIssue(){
		return issue;
	}
	public void setTradetype(int tradetype){
		this.tradetype=tradetype;
	}
	public int getTradetype(){
		return tradetype;
	}
	public void setTradeamount(String tradeamount){
		this.tradeamount=tradeamount;
	}
	public String getTradeamount(){
		return tradeamount;
	}
	public void setSourceid(int sourceid){
		this.sourceid=sourceid;
	}
	public int getSourceid(){
		return sourceid;
	}
	public void setSourcename(String sourcename){
		this.sourcename=sourcename;
	}
	public String getSourcename(){
		return sourcename;
	}
	public void setTargetid(int targetid){
		this.targetid=targetid;
	}
	public int getTargetid(){
		return targetid;
	}
	public void setTargetname(String targetname){
		this.targetname=targetname;
	}
	public String getTargetname(){
		return targetname;
	}
	public void setActivityid(int activityid){
		this.activityid=activityid;
	}
	public int getActivityid(){
		return activityid;
	}
	public void setActivityname(String activityname){
		this.activityname=activityname;
	}
	public String getActivityname(){
		return activityname;
	}
	public void setSubmittime(Date submittime){
		this.submittime=submittime;
	}
	public Date getSubmittime(){
		return submittime;
	}
	public void setPaytype(int paytype){
		this.paytype=paytype;
	}
	public int getPaytype(){
		return paytype;
	}
	public void setRemark(String remark){
		this.remark=remark;
	}
	public String getRemark(){
		return remark;
	}
	public void setCreatetime(String createtime){
		this.createtime=createtime;
	}
	public String getCreatetime(){
		return createtime;
	}
	public void setCreateuserid(int createuserid){
		this.createuserid=createuserid;
	}
	public int getCreateuserid(){
		return createuserid;
	}
	public void setCreateusername(String createusername){
		this.createusername=createusername;
	}
	public String getCreateusername(){
		return createusername;
	}
	public void setUpdatetime(Date updatetime){
		this.updatetime=updatetime;
	}
	public Date getUpdatetime(){
		return updatetime;
	}
	public void setUpdateuserid(int updateuserid){
		this.updateuserid=updateuserid;
	}
	public int getUpdateuserid(){
		return updateuserid;
	}
}

