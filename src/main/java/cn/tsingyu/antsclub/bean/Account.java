package cn.tsingyu.antsclub.bean;

import java.io.Serializable;
import java.util.Date;
import java.math.*;

public class Account implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int ownerid;
	private String ownername;
	private int ownertypecode;
	private int accounttypecode;
	private BigDecimal balance;
	private int relategroupid;
	private String remark;
	private Date createtime;
	private int createuserid;
	private Date updatetime;
	private int updateuserid;

	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setOwnerid(int ownerid){
		this.ownerid=ownerid;
	}
	public int getOwnerid(){
		return ownerid;
	}
	public void setOwnername(String ownername){
		this.ownername=ownername;
	}
	public String getOwnername(){
		return ownername;
	}
	public void setOwnertypecode(int ownertypecode){
		this.ownertypecode=ownertypecode;
	}
	public int getOwnertypecode(){
		return ownertypecode;
	}
	public void setAccounttypecode(int accounttypecode){
		this.accounttypecode=accounttypecode;
	}
	public int getAccounttypecode(){
		return accounttypecode;
	}
	public void setBalance(BigDecimal balance){
		this.balance=balance;
	}
	public BigDecimal getBalance(){
		return balance;
	}
	public void setRelategroupid(int relategroupid){
		this.relategroupid=relategroupid;
	}
	public int getRelategroupid(){
		return relategroupid;
	}
	public void setRemark(String remark){
		this.remark=remark;
	}
	public String getRemark(){
		return remark;
	}
	public void setCreatetime(Date createtime){
		this.createtime=createtime;
	}
	public Date getCreatetime(){
		return createtime;
	}
	public void setCreateuserid(int createuserid){
		this.createuserid=createuserid;
	}
	public int getCreateuserid(){
		return createuserid;
	}
	public void setUpdatetime(Date updatetime){
		this.updatetime=updatetime;
	}
	public Date getUpdatetime(){
		return updatetime;
	}
	public void setUpdateuserid(int updateuserid){
		this.updateuserid=updateuserid;
	}
	public int getUpdateuserid(){
		return updateuserid;
	}
}

