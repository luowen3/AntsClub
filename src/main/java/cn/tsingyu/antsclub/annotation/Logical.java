package cn.tsingyu.antsclub.annotation;

public enum Logical {
    AND, OR
}