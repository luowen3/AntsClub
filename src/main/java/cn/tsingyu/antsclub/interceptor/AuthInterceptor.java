package cn.tsingyu.antsclub.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.tsingyu.antsclub.annotation.AuthPassport;

public class AuthInterceptor extends HandlerInterceptorAdapter {
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	AuthPassport authPassport = ((HandlerMethod) handler).getMethodAnnotation(AuthPassport.class);
        
        //没有声明需要权限,或者声明不验证权限
        if(authPassport == null || authPassport.validate() == false){
        	return true;
        }else if(request.getSession().getAttribute("username") != null) {  
	        //更好的实现方式的使用cookie  
	        return true;  
	    }  
	    //4、非法请求 即这些请求需要登录后才能访问  
	    //重定向到登录页面  
	    response.sendRedirect("/login");  
	    return false;  
    }
}