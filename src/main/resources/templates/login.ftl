<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<#include "/public/header.ftl"/>
<body>
<#include "/public/nav.ftl"/>
	<div class="container" style="min-height: 350px;padding-top:5px;margin-top:62px;">
		<div class="clearfix"></div>
		<div class="panel panel-default">
	  		<div class="panel-body">
	  			<div class="row">
					<p class="lead" style="border-bottom: 1px solid #eee;padding: 0 10px;">用户登录 </p>
		    		<div class="col-md-offset-1 col-lg-7 col-xs-12">
						<br>
						<form id="form_login" class="form-horizontal" action="/login/runlogin.html" method="post" role="form">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputE">用户名：</label>
								<div class="col-sm-5">
	      							<input type="text" id="uname" name="uname" class="form-control"> 
	    						</div>
								<div class="col-sm-4">
									<span class="help-block">
										<div class="Validform_checktip">用户名为必填项</div>
									</span>
		 						</div>
	  						</div>
	     					<div class="form-group">
								<label class="col-sm-3 control-label" for="inputEm">密码：</label>
								<div class="col-sm-5">
	      							<input type="password" placeholder="" name="pwd" id="pwd" class="form-control"> 
	    						</div>
								<div class="col-sm-4">
									<span class="help-block">
										<div class="Validform_checktip">密码为必填项</div>
									</span>
	 							</div>
	  						</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-3">
									<span class="btns green btn-block" data-loading-text="正在提交..." id="btnSubmit">登录</span>
	    						</div>
	  						</div>
	  						<div class="form-group">
	  							<div class="col-sm-offset-3 col-sm-5">
	  								没有帐号？点击 <a href="register"><strong>注册</strong> </a>	
	  							</div>
	  						</div>
	    				</form>
					</div><!-- end col-8-->
					<div class="col-xs-12 col-sm-6 col-md-4" id="Rightbar">
						<div class="box">
							<div class="cell">登录说明</div>
							<div class="inner">
							  <h5>1、登录名和密码是必填项</h5>
							  <h5>2、如果您注册时有输入昵称，则登陆和报名后显示昵称，否则显示登录名</h5>
							  <h5>3、输入完成后，点击回车即可正确提交</h5>
							  <h5>4、登录成功后会转向首页</h5>
							</div>
						</div>
					</div>
	      		</div><!-- end row -->
			</div>
		</div><!-- "panel end -->
	</div>
<#include "/public/footer.ftl"/>
</body>
</html>