<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<#include "/public/header.ftl"/>
<style>
.main{
	min-height: 350px;
	padding-top:5px;
	margin-top:62px;
}
.input-group[class*=col-] {
    float: left;
    padding-left: 15px;
    padding-right: 15px;
}
.lead{
	border-bottom: 1px solid #eee;
	padding: 0 10px;
}
</style>
<body>
<#include "/public/nav.ftl"/>
	<div class="container main">
		<div class="clearfix"></div>
		<div class="panel panel-default">
	  		<div class="panel-body">
	  			<div class="row">
					<p class="lead">用户注册 </p>
		    		<div class="col-lg-7 col-xs-12">
						<br>
						<form id="form_regis" class="form-horizontal" action="register" method="post" role="form">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputE">手机号：</label>
								<div class="col-sm-5">
	      							<input type="text" id="uname" name="uname" class="form-control"  datatype="m" ajaxurl="checkMobile" nullmsg="请填写手机号" errormsg="手机号已注册，忘记密码可找回"> 
	    						</div>
								<div class="col-sm-4">
									<span class="help-block">
										<div class="Validform_checktip">必填项</div>
									</span>
	 							</div>
	  						</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputE">验证码：</label>
							   	<div class="input-group col-sm-5">
							      <input type="text" class="form-control" datatype="n4-4" nullmsg="请填写手机收到的验证码" errormsg="请填写手机收到的验证码">
							      <span class="input-group-btn">
							        <button class="btn btn-success" type="button">获取验证码</button>
							      </span>
							    </div>
								<div class="col-sm-4">
									<span class="help-block">
										<div class="Validform_checktip">必填项,请填写手机收到的验证码</div>
									</span>
	 							</div>
	  						</div>
	     					<div class="form-group">
								<label class="col-sm-3 control-label" for="inputEm">密码：</label>
								<div class="col-sm-5">
	      							<input type="password" placeholder="" name="pwd" id="pwd" class="form-control" datatype="s6-16" nullmsg="请设置密码！" errormsg="为了安全，密码要6位以上"> 
	    						</div>
								<div class="col-sm-4">
									<span class="help-block">
										<div class="Validform_checktip">必填项</div>
									</span>
	 							</div>
	  						</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputEmail">昵称：</label>
								<div class="col-sm-5">
									<input name="nickname" id="nickname" class="form-control" ajaxurl="checkname" errormsg="昵称已被使用，换一个吧"> 
								</div>
								<div class="col-sm-4">
									<span class="help-block">
										<div class="Validform_checktip">给自己取个响亮的名号吧</div>
									</span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-3">
									<span id="submit_form" class="btns green btn-block" data-loading-text="正在提交..." style="text-align:center;">立刻注册</span>
	    						</div>
	  						</div>
	  						<div class="form-group">
	  							<div class="col-sm-offset-3 col-sm-5">
	  								已有帐号？点击 <a href="login"><strong>登录</strong> </a>	
	  							</div>
	  						</div>
	    				</form>
					</div><!-- end form-->
					<div class="col-xs-12 col-sm-6 col-md-4  col-lg-offset-1" id="Rightbar">
						<div class="box">
							<div class="cell">验证手机号的原因：</div>
							<div class="inner">
							  <h5>1、方便通知，偶尔会有紧急情况（比如取消活动等），需要联系到报名的人</h5>
							  <h5>2、方便找回密码，也不会忘记用户名</h5>
							  <h5>3、防止恶意灌水</h5>
							  <h5>我们承诺对个人信息保密，非紧急情况不会打扰您</h5>
							  <h5>希望新人理解，谢谢</h5>
							</div>
						</div>
					</div>
	      		</div><!-- end row -->
			</div>
		</div><!-- "panel end -->
	</div>
	<#include "/public/footer.ftl"/>
	<script src="js/Validform_v5.3.2_min.js"></script>
	<script>
		$(function () {
			$("#form_regis").Validform({
				tiptype:2
			});
			
			$('#submit_form').click(function(){
				$('#form_regis').submit();
			});

			document.onkeydown = function(e){ 
			    var ev = document.all ? window.event : e;
			    if(ev.keyCode==13) {
			    	$('#form_regis').submit();
			     }
			}
		});
	</script>
</body>
</html>