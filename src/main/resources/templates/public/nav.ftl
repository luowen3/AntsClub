<!-- Static navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">精羽门</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
      	<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">发布 <span class="caret"></span></a>
          <ul class="dropdown-menu">
    		<li id="nav_act_add"><a href="{:U('Activity/add')}">发起活动</a></li>
	        <li id="nav_topic_add"><a href="{:U('Topic/add')}">发表话题</a></li>
	        <li id="nav_goods_add"><a href="{:U('GoodsInfo/add')}">发布物品</a></li>
	        <li id="nav_video_add"><a href="{:U('Video/add')}">发布视频</a></li>
          </ul>
        </li>
	    	<li id="nav_account"><a href="{:U('Admin/Account/accountList')}">账户管理</a></li>
		<li id='nav_notice'><a href="{:U('Topic/more',array('cid'=>all))}">公告</a></li>
  		<li id='nav_about'><a href="{:U('Topic/view',array('id'=>14))}">关于</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        	<li><a>欢迎</a></li>
        	<li><a href="{:U('User/activity')}">个人信息</a></li>
        	<li><a href="{:U('Login/logout')}">注销</a></li>
        	<li id='nav_login'><a href="login">登录</a></li>
			<li id='nav_reg'><a href="register">注册</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>