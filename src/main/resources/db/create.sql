CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ownerId` int(10) DEFAULT NULL COMMENT '账户所有者id',
  `ownerName` varchar(20) DEFAULT NULL,
  `ownerTypeCode` int(1) DEFAULT '0' COMMENT '所有者类型：0，普通用户；1，群组用户',
  `accountTypeCode` int(1) DEFAULT '0' COMMENT '账户类型：0，金币（现金）；1，铜板（虚拟）',
  `balance` float DEFAULT '0' COMMENT '账户余额',
  `relateGroupId` int(10) DEFAULT '0' COMMENT '关联群组id',
  `remark` varchar(45) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(10) DEFAULT NULL COMMENT '创建者',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` int(10) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `account_unique` (`ownerId`,`accountTypeCode`,`relateGroupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='账户表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `typeCode` varchar(36) DEFAULT NULL COMMENT '活动类型代码',
  `typeName` varchar(36) DEFAULT NULL COMMENT '活动类型名称',
  `payTypeCode` int(1) DEFAULT NULL COMMENT '付费类型：0，预付费；1，现场付费；2，AA付费；3，免费',
  `payTypeName` varchar(20) DEFAULT NULL,
  `payAmount` float DEFAULT NULL COMMENT '付费金额',
  `title` varchar(100) NOT NULL COMMENT '活动名称',
  `startTime` datetime NOT NULL COMMENT '开始时间',
  `endTime` datetime NOT NULL COMMENT '结束时间',
  `address` varchar(100) NOT NULL COMMENT '活动地点',
  `detail` varchar(300) DEFAULT NULL COMMENT '活动详情',
  `maxNum` int(3) DEFAULT NULL COMMENT '限制人数',
  `minNum` int(3) DEFAULT NULL COMMENT '最少人数',
  `status` int(1) DEFAULT '1' COMMENT '活动状态，1：有效；0：无效',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` varchar(36) DEFAULT NULL COMMENT '创建者',
  `createUserName` varchar(45) DEFAULT NULL COMMENT '创建者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` varchar(36) DEFAULT NULL COMMENT '更新者',
  `updateUserName` varchar(45) DEFAULT NULL COMMENT '更新者名称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='活动表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '话题分类id,对应category的id',
  `title` varchar(45) DEFAULT NULL COMMENT '话题标题',
  `content` text COMMENT '话题内容',
  `createTime` datetime DEFAULT NULL,
  `createUserId` int(11) DEFAULT NULL,
  `createUserName` varchar(45) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updateUserId` int(11) DEFAULT NULL,
  `updateUserName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='话题表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `trade` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `issue` varchar(100) DEFAULT NULL COMMENT '交易事项',
  `tradeType` int(1) DEFAULT NULL COMMENT '交易类型：0：财务充值和退款；1：取消活动时退款; 2：报名活动或者更新活动报名时消费；3：购买商品时消费',
  `tradeAmount` varchar(45) DEFAULT NULL COMMENT '交易金额',
  `sourceId` int(10) DEFAULT NULL COMMENT '源账户id',
  `sourceName` varchar(45) DEFAULT NULL COMMENT '源账户姓名',
  `targetId` int(10) DEFAULT NULL COMMENT '目标账户id',
  `targetName` varchar(45) DEFAULT NULL COMMENT '目标账户姓名',
  `activityId` int(11) DEFAULT NULL COMMENT '活动id，用于参加活动，扣费的时候，记录活动信息',
  `activityName` varchar(45) DEFAULT NULL COMMENT '活动名称，用于参加活动，扣费的时候，记录活动信息',
  `submitTime` datetime DEFAULT NULL COMMENT '交易他提交时间（用户向管理员付款的时间，管理员向用户退款的时间）',
  `payType` int(1) DEFAULT '0' COMMENT '交易方式：0，金币；1，铜板',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(10) DEFAULT NULL COMMENT '创建者',
  `createUserName` varchar(45) DEFAULT NULL COMMENT '创建者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` int(10) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='账户日志表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tweet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '上级动弹id',
  `content` varchar(255) DEFAULT NULL COMMENT '动弹内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(45) DEFAULT NULL COMMENT '创建者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(45) DEFAULT NULL COMMENT '更新者名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='动弹表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uname` varchar(100) NOT NULL COMMENT '用户名',
  `pwd` varchar(100) NOT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '注册邮箱',
  `nickname` varchar(100) DEFAULT NULL COMMENT '昵称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime DEFAULT NULL COMMENT '最后修改时间',
  `role` int(1) NOT NULL COMMENT '角色',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像，只存储middle大小的，large和small通过字符串替换获得',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uname` (`uname`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nickname` (`nickname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户表' AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `user_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activityId` int(11) NOT NULL COMMENT '活动ID',
  `userId` varchar(36) NOT NULL COMMENT '用户ID',
  `userName` varchar(100) DEFAULT NULL COMMENT '用户称呼',
  `signTime` datetime DEFAULT NULL COMMENT '报名时间',
  `signName` varchar(20) DEFAULT NULL COMMENT '报名称呼',
  `signNum` tinyint(3) unsigned DEFAULT NULL COMMENT '报名人数',
  `attendNum` int(3) DEFAULT NULL COMMENT '参加人数',
  `status` varchar(10) DEFAULT NULL COMMENT '活动状态',
  `isSign` varchar(1) DEFAULT NULL COMMENT '用于标识本条记录中的用户是否报名，1是0否',
  `isAttend` varchar(1) DEFAULT NULL COMMENT '用于标识本条记录中的用户是否参加活动，1是0否',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户活动表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL COMMENT '视频标题',
  `contest` varchar(45) DEFAULT NULL COMMENT '赛事名称',
  `team` varchar(45) DEFAULT NULL COMMENT '队伍名称',
  `player` varchar(45) DEFAULT NULL COMMENT '队员名称',
  `round` varchar(45) DEFAULT NULL COMMENT '比赛阶段：小组赛，半决赛，决赛等',
  `program` varchar(45) DEFAULT NULL COMMENT '比赛项目：男子单打，女子单打等',
  `date` varchar(45) DEFAULT NULL COMMENT '比赛时间',
  `address` varchar(45) DEFAULT NULL COMMENT '比赛地点',
  `content` text COMMENT '视频内容',
  `createTime` datetime DEFAULT NULL,
  `createUserId` int(11) DEFAULT NULL,
  `createUserName` varchar(45) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updateUserId` int(11) DEFAULT NULL,
  `updateUserName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='视频表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objCode` tinyint(4) DEFAULT NULL COMMENT '评论对象编码：1，活动表；2,物品信息表；3,话题',
  `objId` int(11) DEFAULT NULL COMMENT '评论对象id',
  `pid` int(11) DEFAULT '0' COMMENT '上级评论id',
  `content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(45) DEFAULT NULL COMMENT '创建者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(45) DEFAULT NULL COMMENT '更新者名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='评论表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `pay` (
  `out_trade_no` varchar(100) NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `createUserId` int(10) NOT NULL COMMENT '付款用户id，对应user表主键',
  `targetObjId` int(10) DEFAULT NULL COMMENT '购买对象的id，可能是商品，也可能是活动报名或者其他',
  `targetObjType` int(1) DEFAULT NULL COMMENT '购买对象类型:0.充值；1.活动报名；2.购买商品',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：未支付；1：已支付',
  `callback` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `param` text NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateUserId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`out_trade_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(4) DEFAULT NULL COMMENT '分类id',
  `title` varchar(45) DEFAULT NULL COMMENT '标题',
  `thumb` varchar(100) DEFAULT NULL COMMENT '缩略图',
  `desc` varchar(255) DEFAULT NULL COMMENT '广告简介',
  `detail` text COMMENT '广告详情',
  `status` int(1) DEFAULT NULL COMMENT '状态 0：不可用 1：可用',
  `sort` int(2) DEFAULT '0',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(15) DEFAULT NULL COMMENT '发布人名称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(15) DEFAULT NULL COMMENT '更新者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='广告表' AUTO_INCREMENT=1 ;

--
-- 表的结构 `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `cityId` int(10) DEFAULT NULL,
  `cityName` char(90) DEFAULT NULL,
  `areaId` int(15) DEFAULT '0',
  `areaName` char(60) DEFAULT NULL,
  `address` char(100) DEFAULT NULL,
  `isTop` int(1) DEFAULT '0',
  `name` char(60) DEFAULT NULL,
  `tel` char(60) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`uid`,`areaId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `ad_cat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(90) DEFAULT NULL,
  `pid` int(15) DEFAULT '0',
  `sort` int(5) DEFAULT '0',
  `top` int(5) DEFAULT '0',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(15) DEFAULT NULL COMMENT '发布人名称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(15) DEFAULT NULL COMMENT '更新者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `acid` (`id`,`name`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='广告分类表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `cate` int(3) DEFAULT '0' COMMENT '0为基本设置，1为支付设置，2登录设置,3店铺设置,4积分设置',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(4) DEFAULT NULL COMMENT '分类id',
  `title` varchar(45) DEFAULT NULL COMMENT '标题',
  `price` float DEFAULT NULL COMMENT '价格',
  `thumb` varchar(100) DEFAULT NULL COMMENT '缩略图',
  `desc` varchar(255) DEFAULT NULL COMMENT '商品简介',
  `detail` text COMMENT '商品详情',
  `status` int(1) DEFAULT NULL COMMENT '状态 0：不可销售 1：销售中',
  `sort` int(2) DEFAULT '0',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(15) DEFAULT NULL COMMENT '发布人名称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(15) DEFAULT NULL COMMENT '更新者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `goods_cat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(90) DEFAULT NULL,
  `pid` int(15) DEFAULT '0',
  `sort` int(5) DEFAULT '0',
  `top` int(5) DEFAULT '0',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(15) DEFAULT NULL COMMENT '发布人名称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(15) DEFAULT NULL COMMENT '更新者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `gcid` (`id`,`name`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品分类表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `goods_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `infoTypeCode` int(11) DEFAULT '0' COMMENT '信息类型：0，转让；1，求购；2，赠送；3，交换',
  `title` varchar(45) DEFAULT NULL COMMENT '标题',
  `price` float DEFAULT NULL COMMENT '价格',
  `priceTypeCode` int(11) DEFAULT NULL COMMENT '价格类型：0，明码标价；1，面议',
  `payTypeCode` int(11) DEFAULT NULL COMMENT '付费类型：0，预付费；1，货到付款',
  `detail` text COMMENT '商品详情',
  `contact` varchar(45) DEFAULT NULL COMMENT '联系方式',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建者id',
  `createUserName` varchar(15) DEFAULT NULL COMMENT '发布人名称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateUserId` int(11) DEFAULT NULL COMMENT '更新者id',
  `updateUserName` varchar(15) DEFAULT NULL COMMENT '更新者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='物品信息表' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `order` (
  `oid` int(30) NOT NULL AUTO_INCREMENT,
  `price` float(10,2) DEFAULT NULL,
  `count` int(10) DEFAULT NULL,
  `discount` float(10,2) DEFAULT NULL,
  `pay` float(10,2) DEFAULT NULL,
  `paytype` int(2) DEFAULT '0',
  `ucount` int(10) DEFAULT '0',
  `uid` int(15) DEFAULT NULL,
  `uname` char(60) DEFAULT NULL,
  `pid` char(100) DEFAULT NULL,
  `shopspay` int(11) DEFAULT '0' COMMENT '配送费用',
  `shopname` char(100) DEFAULT NULL,
  `gid` char(90) DEFAULT NULL,
  `order_ctime` int(16) DEFAULT NULL,
  `order_endtime` int(16) DEFAULT NULL,
  `print_time` int(16) DEFAULT NULL,
  `print_name` char(100) DEFAULT NULL,
  `morecontent` char(200) DEFAULT NULL,
  `otel` char(80) DEFAULT NULL,
  `oman` char(100) DEFAULT NULL,
  `oaddress` char(200) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '0：未支付；1：已支付；2：商家已确认；3：已发货；4：客户已确认收货；5：已评价',
  `source` char(40) DEFAULT NULL,
  `couponid` int(30) DEFAULT '0',
  `deliveryTime` datetime DEFAULT NULL COMMENT '送货时间',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`oid`),
  KEY `id` (`oid`,`price`,`count`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 CHECKSUM=1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `order_ext` (
  `oid` int(30) DEFAULT NULL,
  `did` int(6) DEFAULT NULL,
  `fid` int(6) DEFAULT NULL,
  `fname` char(100) DEFAULT NULL,
  `fcid` int(15) DEFAULT NULL,
  `fcname` char(100) DEFAULT NULL,
  `fprice` float(10,2) DEFAULT NULL,
  `fcount` int(10) DEFAULT NULL,
  `prices` float(10,2) DEFAULT NULL,
  `muid` int(15) DEFAULT NULL,
  `muname` char(100) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `fenliang` char(100) DEFAULT NULL,
  `kouwei` char(100) DEFAULT NULL,
  `call_time` int(16) DEFAULT NULL,
  `print_time` int(16) DEFAULT NULL,
  `end_time` int(16) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `gid` char(64) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





